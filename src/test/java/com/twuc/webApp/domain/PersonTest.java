package com.twuc.webApp.domain;

import com.twuc.webApp.Person;
import com.twuc.webApp.PersonRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


@DataJpaTest(showSql = false)
@DirtiesContext
public class PersonTest {

    @Autowired
    private EntityManager em;

    @Autowired
    private PersonRepository repository;

    @Test
    void dummy_person() {
        Person d = new Person(1L, "d");
        repository.save(d);
        em.flush();
        em.clear();
        Optional<Person> person = repository.findById(1L);
        assertThat(person.isPresent()).isTrue();
        assertThat(person.get().getFirstName()).isEqualTo("d");
    }
}
